import theme from "styles/Theme"

const pythiaStyles = {
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    margin: 'auto',
  },
  image: {
    minWidth: 200,
    minHeight: 200,
    padding: '20px',
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },

};

export default pythiaStyles;
