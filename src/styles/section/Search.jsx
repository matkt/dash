
const searchStyles = {
  fab: {
    margin: '0px',
    top: 'auto',
    right: '20px',
    bottom: '20px',
    left: 'auto',
    position: 'fixed',
    zIndex: 100,
  }

};

export default searchStyles;
