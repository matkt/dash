
import StorePair from "views/section/StorePair";
import Signin from "views/section/Signin";
import AddDoctor from "views/section/AddDoctor";
import AddDonor from "views/section/AddDonor";
import AddReceiver from "views/section/AddReceiver";
import Search from "views/section/Search";

var indexRoutes = [
  {
    path: "/create/signin",
    name: "Signin",
    component: Signin
  },
  {
    path: "/create/doctor",
    name: "Add a Doctor",
    component: AddDoctor
  },
  {
    path: "/create/donor",
    name: "Add a Donor",
    component: AddDonor
  },
  {
    path: "/create/receiver",
    name: "Add a receiver",
    component: AddReceiver
  },
  {
    path: "/create/save",
    name: "Store a pair",
    component: StorePair
  },
  {
    path: "/search",
    name: "Search a pair",
    component: Search
  }
];

export default indexRoutes;
