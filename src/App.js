import React, { Component } from "react";
import compose from "recompose/compose";

import getWeb3 from "./utils/getWeb3";

//redux
import { connect } from "react-redux";
import { initWeb3Action } from "redux/actions/ContractAction";

import Main from "./views/main/Main";
import "./App.css";

class App extends Component {

  componentDidMount = async () => {
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      //const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      //const networkId = await web3.eth.net.getId();

      // Set web3, accounts
      this.props.initWeb3Action(web3);

    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };

  render() {
    if (!this.props.web3) {
      return <div>Loading Web3, accounts, and contract...</div>;
    }


    return (
      <div className="App">
        <Main/>
      </div>
    );

  }
}


const mapStateToProps = state => {
  return {
    web3: state.contractReducer.web3,
  };
};

const mapDispatchToProps = dispatch => ({
  initWeb3Action: web3 => dispatch(initWeb3Action(web3))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(App);
