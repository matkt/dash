import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = theme => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing.unit,
  },
  iconButtonAvatar: {
    padding: 4,
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
});


function Header(props) {
  const { classes, onDrawerToggle } = props;

  return (
    <React.Fragment>
      <AppBar color="primary" position="sticky" elevation={0}>
        <Toolbar>
          <Grid container spacing={8} alignItems="center">
            <Hidden smUp>
              <Grid item>
                <IconButton
                  color="inherit"
                  aria-label="Open drawer"
                  onClick={onDrawerToggle}
                  className={classes.menuButton}>
                  <MenuIcon />
                </IconButton>
              </Grid>
            </Hidden>
            <Grid item xs />
            <Grid item>
              <Typography className={classes.link} component="a" href="#">
                Go to docs
              </Typography>
            </Grid>
            <Grid item>
              <Tooltip title="Alerts • No alters">
                <IconButton color="inherit">
                  <NotificationsIcon />
                </IconButton>
              </Tooltip>
            </Grid>
            <Grid item>
              <IconButton color="inherit" className={classes.iconButtonAvatar}>
                <Avatar className={classes.avatar} src="/static/images/avatar/1.jpg" />
              </IconButton>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <AppBar
        component="div"
        className={classes.secondaryBar}
        color="primary"
        position="static"
        elevation={0}
      >
        <Toolbar>
          <Grid container alignItems="center" spacing={8}>
            <Grid item xs>
              <Typography color="inherit" variant="h5">
                Kidney tool
              </Typography>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <AppBar
        component="div"
        className={classes.secondaryBar}
        color="primary"
        position="static"
        elevation={0}>
        {loadSubMenuItem(this)}
      </AppBar>
    </React.Fragment>
  );
}

function loadSubMenuItem(instance) {
  var pathArray = window.location.pathname.split('/');
  switch (pathArray[1]) {
    case "create":
      return (createSubMenuItem(instance));
    default:
      return (
        <Tabs></Tabs>
      );
  }
}

function createSubMenuItem(instance) {
  var pathArray = window.location.pathname.split('/');
  var index = pathArray[2]==="signin"?0:pathArray[2]==="doctor"?1:pathArray[2]==="donor"?2:pathArray[2]==="receiver"?3:pathArray[2]==="save"?4:0;
  return (
        <Tabs value={index} textColor="inherit">
          <Tab textColor="inherit" label="Sign-in" {...{ to: "/create/signin" }} component={Link} button={true} />
          <Tab textColor="inherit" label="Add Doctor" {...{ to: "/create/doctor"  }} component={Link} button={true} />
          <Tab textColor="inherit" label="Add Donor" {...{ to: "/create/donor"  }} component={Link} button={true} />
          <Tab textColor="inherit" label="Add Receiver" {...{ to: "/create/receiver"  }} component={Link} button={true} />
          <Tab textColor="inherit" label="Save" {...{ to: "/create/save"  }} component={Link} button={true} />
        </Tabs>
      );
}


Header.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(Header);
