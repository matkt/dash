import React from 'react';
import compose from "recompose/compose";

import PropTypes from 'prop-types';

//component
import Paper from '@material-ui/core/Paper';
import ButtonBase from '@material-ui/core/ButtonBase';
import TextField from '@material-ui/core/TextField';
import Fab from '@material-ui/core/Fab';
import NavigationIcon from '@material-ui/icons/Navigation';
import Grid from '@material-ui/core/Grid';

//style
import { withStyles } from '@material-ui/core/styles';
import signinStyle from "styles/section/Signin";

//assets
import immersion from "assets/img/immersion.png";

//redux
import { connect } from "react-redux";


class Signin extends React.Component {


  componentWillMount = async () => {


  };

  render() {

    const { classes } = this.props;


    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Grid container spacing={16}>
            <Grid item>
              <ButtonBase className={classes.image}>
                <img className={classes.img} alt="complex" src={immersion} />
              </ButtonBase>
            </Grid>
            <Grid item xs={12} sm container>
              <center>
                <h1>Sign-in</h1>
                <h4>Enter you public address</h4>
                <TextField
                  id="outlined-full-width"
                  label="Public Address"
                  style={{ margin: 8 }}
                  helperText="Your public address (check Metamask)"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <br />
                <br />
                <h4>In order to use the Hellhound service you have to be connected</h4>
                <br/>
                <Fab variant="extended" color="secondary" aria-label="Add" className={classes.margin}>
                  <NavigationIcon className={classes.extendedIcon} />
                  Sign-in
                </Fab>
              </center>
            </Grid>
          </Grid>
        </Paper>
      </div>
    );
  }

}

Signin.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  return {
    web3: state.contractReducer.web3,
  };
};


export default compose(
  withStyles(signinStyle),
  connect(
    mapStateToProps
  )
)(Signin);
