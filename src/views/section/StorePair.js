import React from 'react';

import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';

//style
import { withStyles } from '@material-ui/core/styles';
import storePairStyle from "styles/section/StorePair";


class StorePair extends React.Component {

  componentDidMount = async () => {

  };

  render() {

    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <h1>test4</h1>
        </Paper>
      </div>

    );
  }

}

StorePair.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(storePairStyle)(StorePair);
