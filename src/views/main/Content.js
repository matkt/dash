import React from 'react';
import PropTypes from 'prop-types';

import { Route, Switch, Redirect } from "react-router-dom";
import indexRoutes from "routes/routes";

import { withStyles } from '@material-ui/core/styles';
import contentStyle from "styles/main/Content";

function Content(props) {

  return (
        <Switch>
          {indexRoutes.map((prop, key) => {
            return (
              <Route path={prop.path} key={key} component={prop.component} />
            );
          })}
          <Redirect from="/" to="/create/signin" />
        </Switch>
  );

}

Content.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(contentStyle)(Content);
