import { combineReducers } from "redux";
import contractReducer from "./ContractReducer";

export default combineReducers({
  contractReducer
});
