export default (state = {
  web3: null,
}, action) => {
 switch (action.type) {
  case 'INIT_WEB3':
   return {
      web3: action.payload,
   }
  default:
   return state
 }
}
