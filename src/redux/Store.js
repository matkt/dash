import { compose, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/RootReducer';

let retrievedState;
try {
  retrievedState = localStorage.getItem('dashboard');
  if (retrievedState === null){
    retrievedState = {};
  }
  retrievedState = JSON.parse(retrievedState);
  console.log("retrievedState ", retrievedState);
} catch (err){
  retrievedState = {};
}

const persistDataLocally = store => next => action => {
  next(action);
  localStorage['dashboard'] = JSON.stringify(store.getState(), function replacer(key, value) {
    return (key === 'web3') ? undefined: value
  })
}


const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;


export default function configureStore(initialState = {}) {
 return createStore(
   rootReducer,
   retrievedState,
   composeEnhancers(applyMiddleware(thunk, persistDataLocally))
 );
}
