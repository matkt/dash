export const initWeb3Action = (web3) => dispatch => {
    dispatch({
        type: 'INIT_WEB3',
        payload: web3
    })
}
